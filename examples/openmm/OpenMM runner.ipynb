{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Run an OpenMM simulation with Narupa\n",
    "\n",
    "Narupa can run OpenMM simulations in two ways: either using [ASE as an interface](../ase/openmm_nanotube.ipynb), or directly using OpenMM mechanisms. Using the ASE interface offers the most flexibility to customise a workflow; in the [graphene example](..ase/openmm_graphene.ipynb) we control the physics parameter of a running simulation from a jupyter notebook. The ASE interface misses some specific OpenMM feature, though; the most noticeable one being holonomic constraints. Using the OpenMM mechanisms without ASE as an interface gives access to all of OpenMM features, but may require more work for some customisation needs. Here, we demonstrate how to to use the OpenMM mechanisms."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare an OpenMM simulation\n",
    "\n",
    "Running an OpenMM simulation with Narupa requires 2 elements: an OpenMM simulation and a Narupa runner. The simulation is a normal OpenMM simulation that you can prepare how you would normally do. The only constraint is to include the Narupa custom force. Especially with small system, removing the center of mass motion leads to unintuitive behaviours when you interact with the molecules.\n",
    "\n",
    "Here, we prepare a simulation of a polyalanine following instructions adaption from [the OpenMM documentation](http://docs.openmm.org/7.0.0/userguide/application.html#a-first-example)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from simtk import openmm as mm\n",
    "from simtk.openmm import app\n",
    "from simtk import unit\n",
    "\n",
    "from narupa.openmm.imd import add_imd_force_to_system\n",
    "\n",
    "pdb = app.PDBFile('openmm_files/17-ala.pdb')\n",
    "forcefield = app.ForceField('amber99sb.xml', 'tip3p.xml')\n",
    "system = forcefield.createSystem(\n",
    "    pdb.topology,\n",
    "    nonbondedMethod=app.PME,\n",
    "    nonbondedCutoff=1 * unit.nanometer,\n",
    "    constraints=app.HBonds,\n",
    "    removeCMMotion=False,\n",
    ")\n",
    "\n",
    "# Add the Narupa custom force to the system. This force\n",
    "# is used by Narupa to transmit the force from the VR\n",
    "# controlers.\n",
    "# The force *must* be added *before* the system is attached\n",
    "# to a simulation.\n",
    "add_imd_force_to_system(system)\n",
    "\n",
    "integrator = mm.LangevinIntegrator(\n",
    "    300 * unit.kelvin,\n",
    "    1 / unit.picosecond,\n",
    "    0.002 * unit.picoseconds,\n",
    ")\n",
    "simulation = app.Simulation(pdb.topology, system, integrator)\n",
    "simulation.context.setPositions(pdb.positions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simulation is a normal OpenMM simulation and can be used as such. The Narupa custom force should not impact the use of the simulation outside of Narupa."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "simulation.minimizeEnergy()\n",
    "simulation.step(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use the simulation with Narupa\n",
    "\n",
    "Narupa works with a client-server architecture. A Narupa runner creates the server and make the link between that server and the simulation. Here, we create a runner and attach to it the simulation we created earlier. Note that the runner adds a reporter to the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from narupa.openmm import OpenMMRunner\n",
    "runner = OpenMMRunner(simulation)\n",
    "runner.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From that point you have the simulation running and a server waiting for clients to connect.\n",
    "\n",
    "Once you are done, you can close the server to free the network port."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "runner.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save a simulation to file\n",
    "\n",
    "Once you have a simulation ready, you may want to save this setup on a file. By doing so, it becomes simpler to reuse the simulation, including with the `narupa-omm-server` command line tool.\n",
    "\n",
    "`narupa.openmm.serializer.serialize_simulation` creates an XML that describes the system, the initial structure, and the integrator. `narupa.openmm.serializer.deserialize_simulation` reads such XML to produce a simulation object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from narupa.openmm import serializer\n",
    "with open('simulation.xml', 'w') as outfile:\n",
    "    outfile.write(serializer.serialize_simulation(simulation))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('simulation.xml') as infile:\n",
    "    simulation_2 = serializer.deserialize_simulation(infile.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use a saved simulation\n",
    "\n",
    "With a simulation saved as an XML file, setting up a Narupa runner becomes much simpler:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from narupa.openmm import OpenMMRunner\n",
    "runner = OpenMMRunner.from_xml_input('openmm_files/17-ala.xml')\n",
    "runner.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Close the server when done with it.\n",
    "runner.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save the trajectory\n",
    "\n",
    "The first benefit of iMD-VR is to see and interect with molecular systems, it is sometimes usefull to save the trajectory as well to run analyses or latter stages of a workflow. Saving the trajectory is done in the regular way for OpenMM simulations: by attaching a reporter. Here we attach a DCD reporter to save the trajectory in the DCD format every 500 frames."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from narupa.openmm import OpenMMRunner\n",
    "runner = OpenMMRunner.from_xml_input('openmm_files/17-ala.xml')\n",
    "\n",
    "dcd_reporter = app.DCDReporter('output.dcd', 500)\n",
    "simulation = runner.simulation\n",
    "simulation.reporters.append(dcd_reporter)\n",
    "\n",
    "runner.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Close the server when done with it.\n",
    "runner.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The DCD reporter does not close the file when the simulation is finished. In some cases, this can prevent to open the trajectory with an other software as long as the jupyter kernel is running. This line closes the file. Note that this will break the reporter in case you want to continue running the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dcd_reporter._dcd._file.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Narupa",
   "language": "python",
   "name": "narupa"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
